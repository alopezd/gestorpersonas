<%@ page contentType="text/html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Gestor de personas</title>
	<link href="../css/normalize.css" type="text/css" rel="stylesheet"/>
	<link href="../css/estilos.css" type="text/css" rel="stylesheet"/>
	<link href="../css/menu.css" type="text/css" rel="stylesheet"/>
</head>
<body>
	<div id="contenedor">
		<header>
			<h1>Gestor de personas</h1>
		</header>
		<%@ include file="/WEB-INF/jsp/trozocutre/menu.jsp"%>
		
		<section>
			<h2>Eliminar personas </h2>
			<form method="post">
				<select name="nombre">
						<%-- Esto SIEEMPRE es problema de JS --%>
						<option value=""> -- seleccione un usuario -- </option>
					<c:forEach items="${personas}" var="u">
						<option value="${u.nombre}">${u.nombre} ${u.apellidos}</option>
					</c:forEach>
				</select>
				<input type="submit" value="Borrar persona"/>
			</form>
			
			<c:if test="${! empty bien}">
				<p> La persona ha sido eliminada del sistema correctamente.</p>
			</c:if>
			
		</section>
		<%@ include file="/WEB-INF/jsp/trozocutre/pie.jsp" %>
	</div>
</body>
</html>