<%@ page contentType="text/html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Gestor de personas</title>
	<link href="../css/normalize.css" type="text/css" rel="stylesheet"/>
	<link href="../css/estilos.css" type="text/css" rel="stylesheet"/>
	<link href="../css/menu.css" type="text/css" rel="stylesheet"/>
</head>
<body>
	<div id="contenedor">
		<header>
			<h1>Gestor de personas</h1>
		</header>
		<%@ include file="/WEB-INF/jsp/trozocutre/menu.jsp"%>
		
		<section>
			<h2>Editar personas </h2>
			<f:form modelAttribute="persona">
				<table class="formulario">
				
					<select name="nombre">
						<%-- Esto SIEEMPRE es problema de JS --%>
						<option value=""> -- seleccione un usuario -- </option>
					<c:forEach items="${personasmod}" var="u">
						<option value="${u.nombre}">${u.nombre} ${u.apellidos}</option>
					</c:forEach>
					</select>
				
					<tr>
						<td><label>Nuevo Apellido</label></td>
						<td><f:input path="apellidos"/></td>
						<td><f:errors path="apellidos"/></td>
					</tr>
				
					<tr>
						<td><label>Nueva Edad</label></td>
						<td><f:input path="edad"/></td>
						<td><f:errors path="edad"/></td>
					</tr>
				
					<tr>
						<td colspan="3" class="unica">
							<input type="submit" value="Editar persona"/>
						</td>
					</tr>	
				</table>
			</f:form>
			
			
			
			<c:if test="${! empty modbien}">
				<p> La persona ha sido modificada del sistema correctamente.</p>
			</c:if>
			
		</section>
		<%@ include file="/WEB-INF/jsp/trozocutre/pie.jsp" %>
	</div>
</body>
</html>