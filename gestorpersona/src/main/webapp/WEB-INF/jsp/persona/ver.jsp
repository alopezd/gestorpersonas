<%@ page contentType="text/html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Gestor de personas</title>
	<link href="../css/normalize.css" type="text/css" rel="stylesheet"/>
	<link href="../css/estilos.css" type="text/css" rel="stylesheet"/>
	<link href="../css/menu.css" type="text/css" rel="stylesheet"/>
</head>
<body>
	<div id="contenedor">
		<header>
			<h1>Gestor de personas</h1>
		</header>
		<%@ include file="/WEB-INF/jsp/trozocutre/menu.jsp"%>
		<section>
			<h2>Listado de personas</h2>
			<table class="datos">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Apellidos</th>
						<th>Edad</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${personas}" var="u">
						<tr>
							<td>${u.nombre}</td>
							<td>${u.apellidos}</td>
							<td>${u.edad}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
	
		</section>
		<%@ include file="/WEB-INF/jsp/trozocutre/pie.jsp" %>
	</div>
</body>
</html>