<%@ page contentType="text/html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Gestor de personas</title>
	<link href="../css/normalize.css" type="text/css" rel="stylesheet"/>
	<link href="../css/estilos.css" type="text/css" rel="stylesheet"/>
	<link href="../css/menu.css" type="text/css" rel="stylesheet"/>
</head>
<body>
	<div id="contenedor">
		<header>
			<h1>Gestor de personas</h1>
		</header>
		<%@ include file="/WEB-INF/jsp/trozocutre/menu.jsp"%>
		<section>
			<h2>Crear nueva persona </h2>
			<f:form modelAttribute="persona">
				<table class="formulario">
					<tr>
						<td><label>Nombre</label></td>
						<td>
							<%-- input type="text" name="nombre" value="${param.nombre}"/--%>
							<f:input path="nombre"/>
						</td>
						<td><f:errors path="nombre"/></td>
					</tr>
				
					<tr>
						<td><label>Apellidos</label></td>
						<td><f:input path="apellidos"/></td>
						<td><f:errors path="apellidos"/></td>
					</tr>
				
					<tr>
						<td><label>Edad</label></td>
						<td><f:input path="edad"/></td>
						<td><f:errors path="edad"/></td>
					</tr>
				
					<tr>
						<td colspan="3" class="unica">
							<input type="submit" value="Crear nueva persona"/>
						</td>
					</tr>	
				</table>
			</f:form>
			
			<%
				Boolean valor=(Boolean)request.getAttribute("bien");
				if (valor!=null) {
			%>
				<p>La persona se ha creado correctamente</p>
			<%
				} 
			%>
			
			<c:if test="${! empty bien}">
				<p>La persona se ha creado correctamente</p>
			</c:if>
			
			<c:if test="${! empty yaExiste}">
				<p class="error">No he podido cear a la persona: el nombre ya existe</p>
			</c:if>
			
			<c:if test="${! empty error}">
				<p class="error">Has escrito algo mal en alguna parte. De nada.</p>
			</c:if>
			
			
	
		</section>
		<%@ include file="/WEB-INF/jsp/trozocutre/pie.jsp" %>
	</div>
</body>
</html>