package com.ana.gestorpersona;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestorpersonaApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestorpersonaApplication.class, args);
	}

}
