package com.ana.gestorpersona.modelo.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

import org.hibernate.validator.constraints.Length;

import com.sun.istack.NotNull;

@Entity
public class Persona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id	
	@NotBlank
	@Length(min = 3, max = 50)
	@Column(length = 50)
	@Pattern(regexp = "^[A-Z�].+$")
	private String nombre;
	
	@NotBlank
	@Length(min = 5, max = 100)
	@Column(length = 100)
	@Pattern(regexp = "^[A-Z�].+$")
	private String apellidos;
	
	@NotNull
	@Positive
	@Column(length = 100)
	private Integer edad;

	public Persona() {
	}

	public Persona(String nombre, String apellidos, Integer edad) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}



	public void setEdad(Integer edad) {
		this.edad=edad;
	}
	
	public Integer getEdad() {
		return edad;
	}

}
