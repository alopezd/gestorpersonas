package com.ana.gestorpersona.modelo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.stereotype.Component;

import com.ana.gestorpersona.modelo.entidad.Persona;


@Component
public class IniciarCosas implements ApplicationRunner{
	
	@Autowired
	private RepositorioPersona ru;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		ru.save(new Persona("Javi","Rodr�guez",40));
		ru.save(new Persona("Ana","P�rez",25));
		ru.save(new Persona("Pedro","Rodr�guez",50));
		ru.save(new Persona("Paco", "Gomez", 27));
		ru.save(new Persona("Sara", "Garcia", 30));
	}
	
}

