package com.ana.gestorpersona.modelo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ana.gestorpersona.modelo.entidad.Persona;

public interface RepositorioPersona extends JpaRepository<Persona, String>{
	
	@Query("Select u From Persona u Where u.nombre=?1 and u.apellidos=?2")
	public Persona buscarPorNombreYApellidos(String nombre, String apellidos);
	
	
}

