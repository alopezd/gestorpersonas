package com.ana.gestorpersona.controlador;

import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ana.gestorpersona.modelo.RepositorioPersona;
import com.ana.gestorpersona.modelo.entidad.Persona;



@Controller
@RequestMapping("/persona")
public class ControladorPersona {
	@Autowired
	private RepositorioPersona ru;
	
	
	
	@RequestMapping("/ver.html")
	public String ver(Map mapa) {
		//La seguridad NO se implementa de esta manera. S�lo es un EJEMPLO
		mapa.put("personas",ru.findAll());
		return "persona/ver";
	}
	
	
	@RequestMapping(value="/crear.html")
	public String crear(Persona u) {
		return "persona/crear";
	}

	@RequestMapping(value="/crear.html", method = RequestMethod.POST, params = {"nombre","apellidos","edad"})
	public String crear(@Validated Persona u, BindingResult errores, Map mapa) {		
		if (errores.hasErrors()) {
			mapa.put("error",true);
			return "persona/crear";
		}
		
		Optional<Persona> op=ru.findById(u.getNombre());
		if (op.isPresent()) {
			mapa.put("yaExiste",true);
		}
		else {
			ru.save(u);
			mapa.put("bien",true);
		}
		return "persona/crear";
	}
	
	@RequestMapping(value="/borrar.html", method = RequestMethod.GET)
	public String borrar(Map mapa) {
		mapa.put("personas",ru.findAll());
		return "persona/borrar";
	}

	@RequestMapping(value="/borrar.html", method = RequestMethod.POST, params = "nombre")
	public String borrar(Persona u, Map mapa) {		
		if (u.getNombre().length()>0) {
			ru.deleteById(u.getNombre());
			mapa.put("bien",true);
		}
		
		return this.borrar(mapa);
	}
	
	@RequestMapping(value="/modificar.html", method = RequestMethod.POST, params = {"nombre","apellidos","edad"})
	public String modificar(Persona u, BindingResult errores, Map mapa) {		
		if (errores.hasErrors()) {
			mapa.put("moderror",true);
			return "persona/crear";
		}
		return this.modificar(mapa);
	}

	@RequestMapping(value="/modificar.html", method=RequestMethod.GET)
	private String modificar(Map mapa) {
		mapa.put("personasmod", ru.findAll());
		return "persona/modificar";
	}

}

