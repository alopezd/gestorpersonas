package com.ana.gestorpersona.controlador;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ControladorMenu {

	
	@RequestMapping("/")
	public String páginaPorDefecto() {
		return "redirect:/menu/bienvenida.html";
	}
	
	@RequestMapping(value="/menu/bienvenida.html", method = RequestMethod.GET)
	public String entrar() {
		return "menu/bienvenida";
	}
}
